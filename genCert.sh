#!/bin/bash
# see https://github.com/ChristianLempa/cheat-sheets/blob/main/misc/ssl-certs.md
# see https://stackoverflow.com/questions/59010148/cannot-read-certificate-file-ca-ssl-mongodb-pem-mongodb-ssl-issue

section() {
	echo -e "\033[0;33m$1\033[0;m"
}

step() {
	echo -e "\033[0;34m$1\033[0;m"
}

oldPWD="$PWD"

args=$@
args=( ${args[@]:0} )

if [ -z "${args[0]}" ]; then
	echo "USAGE: $0 <name of CA group> [<name of certificate group to create certificates for>, ...]"
	exit 10
fi

CAgroup="${args[0]}"
certGroups=( ${args[@]:1} )

if [ -z "$CAgroup" ]; then
	echo "Please provide a CA name. If the folder (CA group) does not exist, it will be created and a default openssl.cnf will be generated."
	exit 11
fi
if ! [ -d "$CAgroup" ]; then
	mkdir -p "$CAgroup" --verbose
fi

cd "$CAgroup"
if ! [ -f "openssl-ca.cnf" ]; then
	step "CA group '$CAgroup' does not have a 'openssl-ca.cnf', generating one ..."
	echo '# modified from https://www.terataki.net/2021/02/11/create-csr-with-a-configfile-using-openssl-on-ubuntu/
# and https://stackoverflow.com/questions/21297139/how-do-you-sign-a-certificate-signing-request-with-your-certification-authority
# combined with https://medium.com/curiouscaloo/how-to-generate-a-wildcard-cert-csr-with-a-config-file-for-openssl-8a6613ab342f
# and with https://www.golinuxcloud.com/openssl-create-client-server-certificate/
# and with https://www.openssl.org/docs/man1.0.2/man5/x509v3_config.html

# See more
# - https://www.openssl.org/docs/man1.0.2/man1/ca.html
# - more complete but not split up example at path "/etc/ssl/openssl.cnf"

[ca]
default_ca             = ca_default

[ca_default]
base_dir               = .
default_days           = 365
default_crl_days       = 30
default_md             = sha3-512
x509_extensions        = ca_extensions
copy_extensions        = copy
crl_extensions         = crl_ext
database               = $base_dir/index.txt
serial                 = $base_dir/serial.txt
unique_subject         = no

[req]
default_bits           = 4096 
default_days           = 365
default_md             = sha3-512
prompt                 = no
x509_extensions        = ca_extensions
distinguished_name     = ca_fields

[ca_fields]
countryName            = "DE"                        # country code (2 letter code)
stateOrProvinceName    = "Hamburg"                   # name of the state ("Bundesland" in German speaking countries)
localityName           = "Hamburg"                   # name of the city
organizationName       = "Test Certfactory, GmbH"    # name of the organization
organizationalUnitName = "ITO"                       # organization unit
commonName             = "Test CA Certificate"       # 
emailAddress           = "admin@example.com"         # Optional: if you want to incorporate an email address

[ca_extensions]
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid:always, issuer
basicConstraints       = critical, CA:true
keyUsage               = keyCertSign, cRLSign

[crl_ext]
issuerAltName          = issuer:copy
authorityKeyIdentifier = keyid:always

[policy_match]
organizationName = match

[signing_policy]
countryName            = optional
stateOrProvinceName    = optional
localityName           = optional
organizationName       = optional
organizationalUnitName = optional
commonName             = supplied
emailAddress           = optional

[signing_req]
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid,issuer
basicConstraints       = CA:FALSE
keyUsage               = digitalSignature, keyEncipherment
' | tee openssl-ca.cnf>/dev/null
else
	step "Group '$CAgroup' already has a 'openssl.cnf', skipping generating one ..."
fi

# certificate generation
caGen() {
	section "--- CA CERTIFICATE GENERATION ---"
	step "generating private key used to protect the CA certificate with ..."
	openssl genrsa -aes256 -out ca-key.pem 4096 # private RSA key for the ca certificate
	
	echo ""
	step "generating the CA certificate"
	openssl req -x509 -config "openssl-ca.cnf" -new -key ca-key.pem -out ca.pem # create certificate for the CA encrypted with 'ca-key.pem' (CSR signing request)
	
	touch index.txt
	echo "01" >serial.txt
	
	section "--- ---"

	#openssl x509 -in ca.pem -text # optional: view the content in human readable form
}

certGen() {
	section "--- ${1^^} CERTIFICATE GENERATION ---"
	step "generate $1 private key ..."
	openssl genrsa -out "$2/$1-key.pem" 4096 # private RSA key for the certificate
	
	echo ""
	step "create signing request ..."
	openssl req -config "$2/openssl-group.cnf" -extensions "$1" -new -key "$2/$1-key.pem" -out "$2/$1-cert.csr"
	
	echo ""
	step "generate $1 certificate and sign against CA certificate ..."
	openssl ca -config "openssl-ca.cnf" -policy "signing_policy" -extensions "signing_req" -cert "ca.pem" -keyfile "ca-key.pem" -out "$2/$1-cert.pem" -outdir "$2" -in "$2/$1-cert.csr" -batch
	
	echo ""
	step "combining $1 certificate and $1 private key ..."
	cat "$2/$1-cert.pem" "$2/$1-key.pem" >"$2/$1.pem"
	step "modifying FACLs' ..."
	chmod o-rwx "$2/$1-cert.pem" "$2/$1-key.pem" "$2/$1.pem" --verbose
	
	echo ""
	step "$1 certificate generation done!"
	section "--- ---"
}

if [ -f "ca-key.pem" ] && [ -f "ca.pem" ]; then
	step "Group '$CAgroup' already has a CA certificate, skipping CA generation ..."
else
	caGen "$CAgroup"
fi

certCategories=( "server" "client" )
for group in "${certGroups[@]}"; do
	if ! [ -d "$group" ]; then
		mkdir -p "$group" --verbose
	fi
	if ! [ -f "$group/openssl-group.cnf" ]; then
		step "Category group '$group' does not have a 'openssl-group.cnf', generating one ..."
		echo '# based on https://stackoverflow.com/questions/21297139/how-do-you-sign-a-certificate-signing-request-with-your-certification-authority
# combined with https://mcilis.medium.com/how-to-create-a-client-certificate-with-configuration-using-openssl-89214dca58ec
# and with https://www.golinuxcloud.com/openssl-create-client-server-certificate/
# and with https://www.openssl.org/docs/man1.0.2/man5/x509v3_config.html

# See more
# - https://www.openssl.org/docs/man1.0.2/man1/ca.html
# - more complete but not split up example at path `/etc/ssl/openssl.cnf`
[req]
default_bits           = 4096 
prompt                 = no
distinguished_name     = cert_fields

[cert_fields]
countryName            = "DE"                        # country code (2 letter code)
stateOrProvinceName    = "Hamburg"                   # name of the state ("Bundesland" in German speaking countries)
localityName           = "Hamburg"                   # name of the city
organizationName       = "Test Certfactory, GmbH"    # name of the organization
organizationalUnitName = "ITO"                       # organization unit
commonName             = "Test Certificate"          # 
emailAddress           = "admin@example.com"         # Optional: if you want to incorporate an email address

[server]
basicConstraints       = CA:FALSE
nsCertType             = server
nsComment              = "Local test server certificate by Test Certfactory"
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage               = critical, digitalSignature, keyEncipherment
extendedKeyUsage       = serverAuth
subjectAltName         = @alternative_names

[alternative_names]
DNS.1 = *
#DNS.2 = <domain>
#DNS.<n> = <domain>

[client]
basicConstraints       = CA:FALSE
nsCertType             = client, email
nsComment              = "Local test client certificate by Test Certfactory"
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid,issuer
keyUsage               = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage       = clientAuth, emailProtection
' | tee "$group/openssl-group.cnf">/dev/null
	fi
	
	for certCategory in "${certCategories[@]}"; do
		step "attempting to generate $certCategory certificate in group $group ..."
		if [ -f "$certCategory-key.pem" ] && [ -f "$certCategory.pem" ]; then
			step "$certCategory certificate found, skipping $certCategory certificate generation ..."
			continue
		fi
		certGen "$certCategory" "$group"
	done
done

cd "$oldPWD"
