# EncryptEasy
Make it as easy as pi to generate SSL/TLS certificates.

This helps you to set up your very own CA to work with trustworthy SSL/TLS certificates. They are trustworhy in the manner you allow them to be. With the CA in hand you can start creating a bunch of certificates signed against the CA.

This does not replace proper certificate management. This repository will be extended bit by bit to help in this manner. Also with good practises. Some to note:

- encrypt, remove your private keys or save them encrypted in an offline storage.
- setup File Access Control Lists
- sandbox applications (be conversative: just give them as much as needed and as less as possible)

## How does this work

You need to do some work upfront before being able to call the script `genCert.sh` (I know that Linux does not need filename extensions). These tasks are easy.

1. Create a directory with a name containing no whitespaces, only numbers and western alphabetic characters
2. Copy `crs-template.cnf` as `csr.cnf` in the directory created.
3. Edit it: Look at the [OpenSSL configuration reference](https://www.openssl.org/docs/man1.1.1/man5/config.html) and at the [OpenSSL x509 configuration reference](https://www.openssl.org/docs/man1.0.2/man5/x509v3_config.html).

## Configuration examples


## Troubleshooting
A list of possible errors and how to solve them while creating a `openssl.cnf` file:
- [there needs to be defined a directory for new certificate to be placed in](http://certificate.fyicenter.com/2134_OpenSSL_ca_Error_..._directory_for_new_certificate_..._.html)
- [variable lookup failed for ca::default_ca](http://certificate.fyicenter.com/2133_OpenSSL_ca_Error_lookup_failed_for_ca_default_ca_.html)
- ...

## Alternatives
- https://github.com/sgallagher/sscg