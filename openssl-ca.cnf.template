# modified from https://www.terataki.net/2021/02/11/create-csr-with-a-configfile-using-openssl-on-ubuntu/
# and https://stackoverflow.com/questions/21297139/how-do-you-sign-a-certificate-signing-request-with-your-certification-authority
# combined with https://medium.com/curiouscaloo/how-to-generate-a-wildcard-cert-csr-with-a-config-file-for-openssl-8a6613ab342f
# and with https://www.golinuxcloud.com/openssl-create-client-server-certificate/
# and with https://www.openssl.org/docs/man1.0.2/man5/x509v3_config.html

# See more
# - https://www.openssl.org/docs/man1.0.2/man1/ca.html
# - more complete but not split up example at path '/etc/ssl/openssl.cnf'

[ca]
default_ca             = ca_default

[ca_default]
base_dir               = .
default_days           = 365
default_crl_days       = 30
default_md             = sha3-512
x509_extensions        = ca_extensions
copy_extensions        = copy
crl_extensions         = crl_ext
database               = $base_dir/index.txt
serial                 = $base_dir/serial.txt
unique_subject         = no

[req]
default_bits           = 4096 
default_days           = 365
default_md             = sha3-512
prompt                 = no
x509_extensions        = ca_extensions
distinguished_name     = ca_fields

[ca_fields]
countryName            = "DE"                        # country code (2 letter code)
stateOrProvinceName    = "Hamburg"                   # name of the state ('Bundesland' in German speaking countries)
localityName           = "Hamburg"                   # name of the city
organizationName       = "Test Certfactory, GmbH"    # name of the organization
organizationalUnitName = "ITO"                       # organization unit
commonName             = "Test CA Certificate"       # 
emailAddress           = "admin@example.com"         # Optional: if you want to incorporate an email address

[ca_extensions]
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid:always, issuer
basicConstraints       = critical, CA:true
keyUsage               = keyCertSign, cRLSign

[crl_ext]
issuerAltName          = issuer:copy
authorityKeyIdentifier = keyid:always

[policy_match]
organizationName = match

[signing_policy]
countryName            = optional
stateOrProvinceName    = optional
localityName           = optional
organizationName       = optional
organizationalUnitName = optional
commonName             = supplied
emailAddress           = optional

[signing_req]
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid,issuer
basicConstraints       = CA:FALSE
keyUsage               = digitalSignature, keyEncipherment
